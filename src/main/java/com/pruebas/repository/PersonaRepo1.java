package com.pruebas.repository;

import com.pruebas.demo01.Demo01Application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * Una simple simulación de un proveedor de datos (podría ser el acceso a una BD)
 */

 @Repository // para indicar que supuestamente sirve para acceder a datos
 @Qualifier("Persona1")
public class PersonaRepo1 implements IPersonaRepo {

    private static Logger log = LoggerFactory.getLogger(Demo01Application.class);

    @Override
    public void registrar(String nombre) {
        log.info("Registro exitoso de P1 > " + nombre);
    }
    
}
