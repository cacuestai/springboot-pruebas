package com.pruebas.demo01;

import com.pruebas.service.IPersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Aplicación con arquitectura
 *    -----------
 *    |   App   |
 *    -----------
 *         |
 *    -----------
 *    | Service |
 *    -----------
 *         |
 *    ------------
 *   | Repository |
 *    ------------
 */

// ver https://stackoverflow.com/questions/40384056/consider-defining-a-bean-of-type-package-in-your-configuration-spring-boot

@SpringBootApplication
@ComponentScan("com") // sin esto genera error "required a bean of type..."
public class Demo01Application implements CommandLineRunner {
	
	@Autowired
	private IPersonaService pServicio;

	public static void main(String[] args) {
		SpringApplication.run(Demo01Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		pServicio.registrar("Carlos Cuesta");
	}

}
