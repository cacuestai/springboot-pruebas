package com.pruebas.service;

import com.pruebas.repository.IPersonaRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service // para indicar que hace parte del modelo de negocio
public class PersonaService implements IPersonaService {
    
    @Autowired // buscar alguna instancia ya definida
    @Qualifier("Persona2") // con esto se indica que tipo de PersonaRepo se requiere
    private IPersonaRepo pRepo;

    @Override
    public void registrar(String nombre) {
        pRepo.registrar(nombre);
    }

}
